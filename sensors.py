#Here is where specific sensors go.
#I will try my best to also create spoof behavior for testing. Although that takes time

from gpiozero import MotionSensor as ms
import time
import smbus

#=== YL-40 ===
#For potentiometers, temperature, light, and analog inputs

class yl40:

    def __init__ (self, addr=0x48, driverNum = 1):
        self.addr = addr
        self.bus = smbus.SMBus(driverNum)
    
    def read (self, chn):
        #Which channel to read
        try:
            if chn == 0:
                self.bus.write_byte(self.addr, 0x40)
            if chn == 1:
                self.bus.write_byte(self.addr, 0x41)
            if chn == 2:
                self.bus.write_byte(self.addr, 0x42)
            if chn == 3:
                self.bus.write_byte(self.addr, 0x43)
            self.bus.read_byte(self.addr) #Clears the buffer
        except Exception as e:
            print("Address: {}".format(self.addr))
            print(e)
        return self.bus.read_byte(self.addr)
    
    def write (self, val):
        try:
            temp = val
            temp = int(temp)
            self.bus.write_byte_data(self.addr, 0x40, temp)
        except Exception as e:
            print("Error: Device Address:0x%2X" % address)
            print(e)

    def checkAllChn(self):
        print('AIN0 = {}'.format(self.read(0)))
        print('AIN1 = {}'.format(self.read(1)))
        print('AIN2 = {}'.format(self.read(2)))
        print('AIN3 = {}'.format(self.read(3)))

    def ledLevel(val):
        #Where val is from 0 to 255
        tmp = val
        tmp = tmp*(255-125)/255+125 #Apparently the minimum for LED is 125, convert 0-255 to 125-255
        self.write(tmp)

#==== PIR ====
#Wrapper for the MotionSensor class to deal with multiple/timings


class singlePIR:
    def __init__ (self, pin):
        self.pir = ms(pin)
        self.lastOnTime = 0
        self.lastOffTime = 0

        self.pir.when_motion= self.updateLastOnTime
        self.pir.when_no_motion= self.updateLastOffTime

    def updateLastOnTime (self):
        self.lastOnTime = time.time()

    def updateLastOffTime (self):
        self.lastOffTime = time.time()

    def getCurrentStatus (self):
        return self.pir.motion_detected

    def getLastOnTime (self):
        return self.lastOnTime

    def getLastOffTime(self):
        return self.lastOffTime

        



class basicListen:
    #A basic listener that gets updated by other classes and stores a value and a time

    def __init__ (self, classToListenTo):
        #classToListenTo must have method addObserver() and addListeners()
        self.value = None
        self.lastTime = 0.0
        self.classToListenTo = classToListenTo
        self.classToListenTo.addListener(self.updateValue)

    def updateValue(self, value):
        self.value = value
        self.lastTime = time.time()

class gate:
    #How this works is that each ms class takes queue_len readings per second
    #queue_len defines the number of readings that are stored
    #threshold defines the fraction (0 to 1) of readings in the queue are required to trigger is_activei
    #honestly probably scrap this.

    def __init__ (self, pinOut, pinIn, triggerMax = 0.5, triggerMin = 0.1):
        #where pinOut is the pin of the outside sensor, and vice versa
        #triggerMax is the time window (in seconds) between in/out triggers to be considered as something going in/out
        #triggerMin is the minTime, the other one has to NOT update. Then, it is considered off.

        #In this case, if something runs from out to in, only the the inPIR will call the return function
        #since outPIR will have just shut off, then reset the times.
        #uses the Observer Pattern

        self.listeners = []

        self.outPIR = ms(pinOut)
        self.inPIR = ms(pinIn)

        self.outTime = 0.0 #epoch time since last trigger.
        self.inTime = 0.0

        self.lastIn = 0.0 #Times of the last full in/out
        self.lastOut = 0.0 
        

        self.triggerMax = triggerMax
        self.triggerMin = triggerMin
        
        #Bind the update/checking functions.
        self.outPIR.when_motion = self.updateLastOut
        self.inPIR.when_motion = self.updateLastIn
        
        
    
    def updateLastOut(self):
        #Binds to outPIR as a function to call when updated.
        #outPir will call this function when it detects something
        #Once this function is done, check if lastIn has been updated recently
        #If so update everyone who is listening.
        self.outTime = time.time()
        print('Out PIR Triggered at {}!'.format(self.outTime))
        
        #Check if the inPIR just turned off. If so, then something just ran out.
        deltaTime = self.outTime - self.inTime
        if deltaTime < self.triggerMax and deltaTime > self.triggerMin:
            self.updateListeners('Out')
            self.lastOut = time.time()
                    

    def updateLastIn(self):
        #Binds to inPIR as a function to call when updated.
        #inPir will call this function when it detects something
        #Once this function is done, check if lastIn has been updated recently
        #If so update everyone who is listening.
        self.inTime = time.time()
        print('In PIR Triggered at {}!'.format(self.inTime))
        
        deltaTime = self.inTime - self.outTime
        if deltaTime < self.triggerMax and deltaTime > self.triggerMin:
            self.updateListeners('In')
            self.lastIn = time.time()

    def addListener(self, callbackFunction):
        #Where callbackFunction is a function that recieves a single value to play with.
        print('Listener Added!')
        self.listeners.append(callbackFunction)

    def updateListeners(self, valueToSend):
        #Sends out the value
        print('Sending Out: {}'.format(valueToSend))
        for returnFunc in self.listeners:
            try:
                returnFunc(valueToSend)
            except:
                print('Function {} failed!'.format(returnFunc))
    

    #Spoofing/Testing

    def spoofUpdate(self, value):
        print('Spoofing as something just ran {}!'.format(value))
        if value == 'Out':
            self.updateListeners('Out')
            self.lastOut = time.time()
        if value == 'In':
            self.updateListeners('In')
            self.lastOut = time.time()


