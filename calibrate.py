#For calibration stuff, both getting values and setting them seperately or not
#Each sensor needs a seperate csv file that stores each data
#Thus it will need a setter, getter, remover, and cli
#Use a seperate class to fit and extrapolate data from a specified csv file
import os
from time import sleep
import sensors as s
try:
    import numpy as np
    from numpy.polynomial import polynomial as P
    npOk = True
except:
    'Numpy not avaliable!'
    npOk = False
caliDir = './calibration-data'

#fsr

def fp(filePath):
    return caliDir + '/' + filePath

def appendVal(csvFilePath, readval, equivval):
    #readval is the sensor value, equivval is the calibration data
    #check if directory is there
    if not os.path.isdir(caliDir):
        print("No Calibration Directory, making one.")
        os.mkdir(caliDir)

    if not os.path.exists(fp(csvFilePath)):
        csvFile = open(fp(csvFilePath), 'w')
        csvFile.write('{},{}'.format(readval, equivval))
    else:
        csvFile = open(fp(csvFilePath), 'a')
        csvFile.write('\n{},{}'.format(readval, equivval))

    csvFile.close()

class dataFit:
    def __init__ (self, csvFilePath):
        #Where csvFilePath is the filePath to the calibration csv
        #Read in 
        if npOk:
            np.csvFilePath = csvFilePath
            data = np.genfromtxt(csvFilePath, delimiter=',')
            print(data)
            self.rawVals = data[:, 0]
            self.equiVals = data[:, 1]
            
            #initial fit.
            self.consts = self.getPolyConst()
            self.poly = np.poly1d(self.consts)


    def getPolyConst(self, deg = 4):
        if npOk:
            return np.polyfit(self.rawVals, self.equiVals, deg)

    def evaluateRawVal(self, val):
        return self.poly(val)


            

def runCli(sensorNum = False):
    yl40 = s.yl40(0x48)
    print('Which sensor?\n(1): AIN2 Force Sens. Resist.\n(2): AIN3 Force Sens. Resist.\n(3): Thermometer\n(4): Light Sen. Resist.')
    try:
        sensorNum = int(input())
        if sensorNum == 1:
            csvFilePath = 'ain2frs.csv'
            chn = 2
        elif sensorNum == 2:
            csvFilePath = 'ain3frs.csv'
            chn = 3
        elif sensorNum == 3:
            csvFilePath = 'temp.csv'
            chn = 1
        elif sensorNum == 4:
            csvFilePath = 'lsr.csv'
            chn = 0
        else:
            print("I don't know what that input is. Put in a number from 1-4 please.")
            return
    except:
            print("I don't know what that input is. Put in a number from 1-4 please.")
            return
    
    print('Press Ctrl + C to stop and input data.')
    print(chn)
    while True:
        try:
            rawval = yl40.read(chn)
            print('Raw Value: {}'.format(rawval))
            sleep(0.3)
        except KeyboardInterrupt:
            break

    rawval = yl40.read(chn)
    print('\nRaw Value: {}'.format(rawval))
    while True:
        try:
            equivval = float(input('Actual Value: '))
            break
        except:
            print('Bad Value, try again please.')
    
    print('Is this Ok? {} raw = {} actual'.format(rawval, equivval))
    
    while True:
        try:
            isOk = input('y/n: ')
            break
        except:
            print("'y' or 'n'")

    if isOk == 'y':
        appendVal(csvFilePath, rawval, equivval)
        print('Added!')
    return
    


if __name__ == "__main__":
    runCli()
